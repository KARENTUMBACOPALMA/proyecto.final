import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    
    {
      path: "/",
      name: "home",
      component: () => import("../views/HomeView.vue"),
    },
    
    {
      path: "/App",
      name: "App",
      component: () => import("../views/App.vue"),
    },
    {
      path: "/registro",
      name: "registro",
      component: () => import("../views/registro.vue"),
    },
    {
      path: "/cambiocontra",
      name: "cambiocontra",
      component: () => import("../views/cambiocontra.vue"),
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/login.vue"),
    },
    {
      path: "/ofertadetrabajo",
      name: "ofertadetrabajo",
      component: () => import("../views/ofertadetrabajo.vue"),
    },
    {
      path: "/horarios",
      name: "horarios",
      component: () => import("../views/horarios.vue"),
    },
    {
      path: "/egresados",
      name: "egresados",
      component: () => import("../views/egresados.vue"),
    },
    {
      path: "/aspirantes",
      name: "aspirantes",
      component: () => import("../views/aspirantes.vue"),
    },
  ],
});

export default router;