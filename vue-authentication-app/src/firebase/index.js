import { initializeApp } from "firebase/app";
import {getAuth} from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBT2gLwxOYviP1HYpcU5LYlkVPVvrNvz-8",
  authDomain: "proyecto-d3f76.firebaseapp.com",
  projectId: "proyecto-d3f76",
  storageBucket: "proyecto-d3f76.appspot.com",
  messagingSenderId: "638740484082",
  appId: "1:638740484082:web:a8dfb5e02eeeb85e8ca56b"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export {auth};