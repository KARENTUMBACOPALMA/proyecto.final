import {defineStore} from "pinia";

import {auth} from "../firebase";

import { createUserWithEmailAndPassword } from "firebase/auth";

export const useUserStore = defineStore("user",{

state: () => {

    return{
user: null,


    };
},
actions: {
async register (email, password){
try {
    await createUserWithEmailAndPassword(auth, email, password);

} catch(error) {
switch(error.code){
case "auth/email-already-in-use":
    alert("El correo ya está en uso");
    break;
    case "auth/invalid-email":
        alert ("El correo electrónico no es válido");
        break;

}
return;
}
this.user = auth.currentUser;
console.log("Usuario registrado satisfactoriamente");



},

},
});